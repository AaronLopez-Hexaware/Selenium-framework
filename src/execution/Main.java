package execution;

import testCases.GoogleSearchTest;
import testCases.UdemyTest;
import utilities.ReadDatasheet;

public class Main {
	public static void main(String[] args) throws Exception 
	{ 	
		ReadDatasheet dS = new ReadDatasheet();
		String[][] inputData = dS.readExcelCsv("GoogleSearch.csv");
		GoogleSearchTest test = new GoogleSearchTest("chrome",inputData, dS);
		//UdemyTest test = new UdemyTest("chrome");
	}
}
