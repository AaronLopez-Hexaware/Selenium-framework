package objectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class GoogleObject {

	public static class Homepage{

		public static WebElement Google_TextBox(WebDriver driver) {

			WebElement element = null;

			element = driver.findElement(By.id("lst-ia"));

			return element;
		}
		
		public static WebElement Google_GmailLink(WebDriver driver) {
			WebElement element = null;
			element = driver.findElement(By.xpath("//a[text()=\"Gmail\"]"));
			return element;
		}		
	}
	
	public static class ResultsPage{
		
		public static WebElement Hexaware_site(WebDriver driver) {
			WebElement element = null;
			element = driver.findElement(By.partialLinkText("Hexaware - IT, BPO,"));
			
			return element;
		}
	}

}
