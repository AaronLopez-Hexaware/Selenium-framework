package objectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class UdemyObject {

	public static class Home{
		
		public static WebElement btnSignUp(WebDriver driver) {
			
			WebElement element = null;
			element = driver.findElement(By.xpath("//*[@id=\"udemy\"]/div[1]/div[2]/div[1]/div[4]/div[5]/require-auth/div/a"));
			return element;
		}
		
	}
}
