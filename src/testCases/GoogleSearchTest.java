package testCases;

import java.io.IOException;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import objectRepository.GoogleObject;
import utilities.DriverHandler;
import utilities.ReadDatasheet;

public class GoogleSearchTest {

	private WebDriver driver;
	private WebElement element;
	
	public GoogleSearchTest(String browser, String[][] inputData, ReadDatasheet dS) throws InterruptedException, IOException {	
		
		DriverHandler dh = new DriverHandler(browser);
		driver = dh.GetDriverHandler();
		WebElement element = null;
		String result = "";
		
		/*
		 *  Extend Reports
		 *  
		 */
		
		ExtentHtmlReporter htmlReporter;
		ExtentReports extent;
		ExtentTest logger = null;
		
		
		htmlReporter = new ExtentHtmlReporter("reporte.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		extent.setSystemInfo("Host name","Curso de Selenium");
		extent.setSystemInfo("User", "Aaron Lopez");
		extent.setSystemInfo("Enviroment", "Windows 7");
		
		htmlReporter.config().setDocumentTitle("Selenium Tutorial");
		htmlReporter.config().setReportName("Selenium Framework");
		htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
		htmlReporter.config().setTheme(Theme.DARK);
		
		try 
		{
			logger = extent.createTest("Google Search");
			dh.Navigate("www.google.com");
			logger.log(Status.PASS, MarkupHelper.createLabel("Navigate to Google website", ExtentColor.GREEN));
				
		}catch (Exception ex) {
			result = dh.takeSnapshot();
			logger.fail(ex.getMessage(), MediaEntityBuilder.createScreenCaptureFromPath(result).build());
		}
		
		try 
		{
			element = GoogleObject.Homepage.Google_TextBox(driver);
			dh.Type(element, dS.GetLocalDataSheetValue(inputData, 1, "DT_SEARCH"));
			result = dh.takeSnapshot();
			logger.log(Status.PASS, MarkupHelper.createLabel("Type Hexaware" , ExtentColor.GREEN));
			dh.Type(element, Keys.ENTER);
				
		}catch (Exception ex) {
			result = dh.takeSnapshot();
			logger.fail(ex.getMessage(),MediaEntityBuilder.createScreenCaptureFromPath(result).build());
		
		}

		element = GoogleObject.ResultsPage.Hexaware_site(driver);
		dh.ClickOnElement(element);
		
		dh.takeSnapshot();
		dh.Wait(5);
		dh.scroll();

		
		logger = extent.createTest("Google2");
		
		extent.flush();
		
		

	}
}
