package testCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import objectRepository.UdemyObject;
import utilities.DriverHandler;

public class UdemyTest {

	private WebDriver driver;
	private WebElement element;
	
	public UdemyTest(String browserName) {
		
		DriverHandler dh = new DriverHandler(browserName);
		driver = dh.GetDriverHandler();
		
		
		try 
		{
			// Navegar a udemy
			dh.Navigate("www.udemy.com");
			
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		
		element = UdemyObject.Home.btnSignUp(driver);
		dh.ClickOnElement(element);
		
		
	}
}
