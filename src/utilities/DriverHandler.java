package utilities;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import javax.naming.directory.NoSuchAttributeException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.NewSessionPayload;

public class DriverHandler {

	private String driverName;
	private static WebDriver driver;
	private static WebElement element;
	private static JavascriptExecutor js;


	public DriverHandler(String driverName)
	{
		this.driverName = driverName;
	}

	public WebDriver GetDriverHandler()
	{
		String _driverPath;
		switch(driverName.toLowerCase())
		{
		case "chrome":
			_driverPath = "Drivers\\chromedriver.exe"; 
			System.setProperty("webdriver.chrome.driver", _driverPath); 
			ChromeOptions ChromeOptions = new ChromeOptions();
			ChromeOptions.addArguments("disable-extensions");
			ChromeOptions.addArguments("--start-maximized");
			driver = new ChromeDriver(ChromeOptions);	
			break;

		case "firefox":
			_driverPath = "Drivers\\geckodriver.exe";
			System.setProperty("webdriver.gecko.driver", _driverPath);
			FirefoxOptions FxOptions = new FirefoxOptions();
			FxOptions.addArguments("disable-extensions");
			FxOptions.addArguments("--start-maximized");
			driver = new FirefoxDriver(FxOptions);

			break;

		default:
			driver = null;
			System.out.println("Driver not availabe");
			break;			
		}

		js = ((JavascriptExecutor)driver);

		return driver;
	}

	private static WebElement FindElement(String locatorType, String locator) {
		switch (locatorType) {
		case "id":
			element = driver.findElement(By.id(locator));
			break;

		case "name":
			element = driver.findElement(By.name(locator));
			break;

		case "xpath":
			element = driver.findElement(By.xpath(locator));
			break;
		default:
			System.out.println("Invalid locator type");
			break;
		}

		return element;
	}

	// Metodos de Acciones
	public static void Navigate(String url) {
		if(!url.contains("http") || !url.contains("https")) {
			driver.navigate().to("https://" + url);
		}	
	}

	public static void Type(WebElement element, String text) {
		try {
			Highlight(element);
			element.sendKeys(text);

		}catch(ElementNotVisibleException ex) {
			System.out.println(ex.getMessage());
		}
	}

	public static void Type(WebElement element, Keys key) {
		Highlight(element);
		element.sendKeys(key);
	}

	public static void ClickOnElement(WebElement element) {
		Highlight(element);
		element.click();
	}

	public static void scroll(int x, int y) {
		js.executeScript("window.scrollBy(" + x +"," + y + ");");
	}

	public static void Highlight(WebElement element) {
		String border;
		border = "#f00 solid 5px";
		js.executeScript("arguments[0].style.outline = '" + border + "'; ", element);

	}

	public static void scroll() {
		js.executeScript("window.scrollBy(0,document.body.scrollHeight);");
	}

	public static String takeSnapshot() throws IOException {

		String newPath = "";
		try {
			Date fecha = new Date();
			SimpleDateFormat formateador = new SimpleDateFormat("yyyyMMddHHmmssFFF");
			String timestamp = formateador.format(fecha);

			String directory = "c://fotos//";
			TakesScreenshot scrShot = ((TakesScreenshot)driver);
			File file = scrShot.getScreenshotAs(OutputType.FILE);
			newPath = directory + timestamp + ".png";
			File destFile = new File(newPath);
			FileUtils.copyFile(file, destFile);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

		return newPath;
	} 


	public static void MouseOver(WebElement elementHover) {

		try 
		{
			Actions action = new Actions(driver);
			action.moveToElement(elementHover).build().perform();

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}


	public static void MouseOverAndClick(WebElement elementHover, WebElement elementToClick) {

		try 
		{
			Actions action = new Actions(driver);
			action.moveToElement(elementHover).click(elementToClick).build().perform();

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	} 

	public static void SwitchToWindows() {
		String currWin = driver.getWindowHandle();
		Set<String> handles = driver.getWindowHandles();
		handles.remove(currWin);

		for(String winHandle : handles) 
		{
			if(!currWin.equals(winHandle)) {			
				String child = winHandle;
				driver.switchTo().window(child);		
			}
		}
	}

	public static void getAttribute(WebElement element, String attr) throws NoSuchAttributeException, IOException {
		try 
		{
			element.getAttribute(attr);

		}catch (Exception ex) {
			takeSnapshot();
			System.out.println(ex.getMessage());
		}
	}

	public void Wait(int s) throws InterruptedException {
		Thread.sleep(s*1000);
	}



}